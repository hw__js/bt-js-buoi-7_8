var x = 0;
var array = Array();

document.getElementById("clickAdd").onclick = function () {
  //Input element into array
  array[x] = document.getElementById("elementsValue").value * 1;
  x++;
  document.getElementById("elementsValue").value = "";

  // Output all elements in array
  var e = "";

  for (var i = 0; i < array.length; i++) {
    e += array[i] + ", ";
  }
  var listElements = `<p><i class="fa fa-caret-right"></i> Array: ${e} </p>`;
  document.getElementById("showArray").innerHTML = listElements;
};

//====================== EX 1 ========================
document.getElementById("clickShow1").onclick = function () {
  var total = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      total += array[i];
    }
  }
  var resultBT1 = `<p><i class="fa fa-caret-right"></i> Total of all elements in array: ${total} </p>`;
  document.getElementById("resultBT1").innerHTML = resultBT1;
};

//====================== EX 2 ========================
document.getElementById("clickShow2").onclick = function () {
  var count = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      count++;
    }
  }
  var positiveNum = `<p><i class="fa fa-caret-right"></i> Positive integer in array: ${count} </p>`;
  document.getElementById("resultBT2").innerHTML = positiveNum;
};

//====================== EX 3 ========================
document.getElementById("clickShow3").onclick = function () {
  var minValue = array[0];
  for (var i = 0; i < array.length; i++) {
    if (array[i] < minValue) {
      minValue = array[i];
    }
  }
  var minNum = `<p><i class="fa fa-caret-right"></i> Positive integer in array: ${minValue} </p>`;
  document.getElementById("resultBT3").innerHTML = minNum;
};

//====================== EX 4 ========================
document.getElementById("clickShow4").onclick = function () {
  var minPosNumValue = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      minPosNumValue = array[i];
    }
  }

  for (i = 0; i < array.length; i++) {
    if (minPosNumValue > array[i] && array[i] > 0) {
      minPosNumValue = array[i];
    }
  }
  var minPosShow = `<p><i class="fa fa-caret-right"></i> Smallest Positive element in array: ${minPosNumValue} </p>`;
  document.getElementById("resultBT4").innerHTML = minPosShow;
};

//====================== EX 5 ========================
document.getElementById("clickShow5").onclick = function () {
  for (var i = 0; i < array.length - 1; i++) {
    if (array[i] % 2 == 0) {
      var evenLast = array[i];
    }
  }

  if (evenLast % 2 == 0 && evenLast != 0) {
    var evenLastShow = `<p><i class="fa fa-caret-right"></i> Smallest Positive element in array: ${evenLast} </p>`;
  } else {
    var evenLastShow = `<p><i class="fa fa-caret-right"></i> Smallest Positive element in array: -1 </p>`;
  }
  document.getElementById("resultBT5").innerHTML = evenLastShow;
};

//====================== EX 6 ========================
document.getElementById("clickShow6").onclick = function () {
  var pos1 = document.getElementById("pos1").value * 1;
  var pos2 = document.getElementById("pos2").value * 1;

  // way 2
  array[pos1] = array[pos2] + ((array[pos2] = array[pos1]), 0);

  // // way 1
  // swap(pos1, pos2);

  displayArray6();
};

// way 1
// swap function
function swap(x, y) {
  var temp = array[x];
  array[x] = array[y];
  array[y] = temp;
}

// function to output array
function displayArray6() {
  var e = "";

  for (var i = 0; i < array.length; i++) {
    e += array[i] + ", ";
  }
  var listElements = `<p><i class="fa fa-caret-right"></i> Array: ${e} </p>`;
  document.getElementById("resultBT6").innerHTML = listElements;
}

//====================== EX 7 ========================
document.getElementById("clickShow7").onclick = function () {
  // Apply Selection Sort Algorithm in Data Sctructure & Algorithms
  for (var i = 0; i < array.length - 1; i++) {
    for (var j = i + 1; j < array.length; j++) {
      if (array[i] > array[j]) {
        swap(i, j);
      }
    }
  }
  displayArray7();
};

function displayArray7() {
  var e = "";

  for (var i = 0; i < array.length; i++) {
    e += array[i] + ", ";
  }
  var listElements = `<p><i class="fa fa-caret-right"></i> Array: ${e} </p>`;
  document.getElementById("resultBT7").innerHTML = listElements;
}

//====================== EX 8 ========================
document.getElementById("clickShow8").onclick = function () {
  var count = 0;
  for (var i = 0; i < array.length; i++) {
    if (isPrime(array[i]) == 1) {
      count++;
      if (count == 1) {
        var primeNum = `<p><i class="fa fa-caret-right"></i> The first prime number: ${array[i]} </p>`;
        document.getElementById("resultBT8").innerHTML = primeNum;
      }
    }
  }
};

function isPrime(n) {
  if (n < 2) return 0;
  else {
    for (var i = 2; i <= parseInt(Math.sqrt(n)); i++) {
      if (n % i == 0) return 0;
    }
    return 1;
  }
}

//====================== EX 9 ========================
var arrayOfEx9 = [];

document.getElementById("clickShow9").onclick = function () {
  // Input element in array
  var inputValue = document.querySelector("#num9");
  var num_ex9 = inputValue.value * 1;
  arrayOfEx9.push(num_ex9);
  inputValue.value = "";

  // Out all elements in array
  var e = "";

  for (var i = 0; i < arrayOfEx9.length; i++) {
    e += arrayOfEx9[i] + ", ";
  }
  var listElements = `<p><i class="fa fa-caret-right"></i> Array: ${e} </p>`;
  document.getElementById("showArray9").innerHTML = listElements;
};

document.getElementById("integerCount").onclick = function () {
  var count = 0;

  for (var i = 0; i < arrayOfEx9.length; i++) {
    if (Number.isInteger(arrayOfEx9[i])) {
      count++;
    }
  }
  var listElements = `<p><i class="fa fa-caret-right"></i> Has ${count} integer in array </p>`;
  document.getElementById("resultBT9").innerHTML = listElements;
};

//====================== EX 10 ========================
document.getElementById("clickShow10").onclick = function () {
  var positiveNumberCount = 0,
    negativeNumberCount = 0;

  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      positiveNumberCount++;
    } else if (array[i] < 0) {
      negativeNumberCount++;
    }
  }

  if (positiveNumberCount > negativeNumberCount) {
    var compareResult = `<p><i class="fa fa-caret-right"></i> A number of positive numbers > A number of negative numbers </p>`;
    document.getElementById("resultBT10").innerHTML = compareResult;
  } else if (positiveNumberCount < negativeNumberCount) {
    var compareResult = `<p><i class="fa fa-caret-right"></i> A number of positive numbers < A number of negative numbers </p>`;
    document.getElementById("resultBT10").innerHTML = compareResult;
  } else {
    var compareResult = `<p><i class="fa fa-caret-right"></i> A number of positive numbers = A number of negative numbers </p>`;
    document.getElementById("resultBT10").innerHTML = compareResult;
  }
};
